<?php
declare(strict_types=1);

// === Build tree array
// Each element of the tree array is array of type [id, ownerId, name, position]
// * id generates automatically when creates new element
// * ownerId is ID of the parent tree node (or null for root node)
// * name is node description
// * position is integer number which describes the node position in the tree as if it was kept in array
//      position of the root node is 0
//      position of the left child of node calculates as 2 * position + 1
//      position of the right child of node calculates as 2 * position + 2

/**
 * @param string $name
 * @return array
 */
function createNode(string $name): array
{
    static $id = 0;

    $id++;

    return [
        'id' => $id,
        'ownerId' => null,
        'name' => $name,
        'position' => null,
    ];
}

/**
 * @param array $tree
 * @param string $name root node name
 *
 * @return array
 */
function startTree(array &$tree, string $name): array
{
    $root = createNode($name);
    $root['position'] = 0;

    $tree[] = $root;

    return $root;
}

/**
 * Add node to the left
 *
 * @param array $tree
 * @param array $node
 * @param string $name
 *
 * @return array
 * @see createNode
 */
function addLeft(array &$tree, array $node, string $name): array
{
    $newNode = createNode($name);
    $newNode['ownerId'] = $node['id'];
    $newNode['position'] = 2 * $node['position'] + 1;

    $tree[] = $newNode;

    return $newNode;
}

/**
 * Add node to the left
 *
 * @param array $tree
 * @param array $node
 * @param string $name
 *
 * @return array
 * @see createNode
 */
function addRight(array &$tree, array $node, string $name): array
{
    $newNode = createNode($name);
    $newNode['ownerId'] = $node['id'];
    $newNode['position'] = 2 * $node['position'] + 2;

    $tree[] = $newNode;

    return $newNode;
}

/*

       ROOT(1)
      /    \
     L2     R4
    /  \   /  \
   L3  R6 L5  R8
         \    /
         R7  L9
*/

$treeArray = [];
$root = startTree($treeArray, 'ROOT');
$l2 = addLeft($treeArray, $root, 'L2');
$l3 = addLeft($treeArray, $l2, 'L3');
$r4 = addRight($treeArray, $root, 'R4');
$l5 = addLeft($treeArray, $r4, 'L5');
$r6 = addRight($treeArray, $l2, 'R6');
$r7 = addRight($treeArray, $r6, 'R7');
$r8 = addRight($treeArray, $r4, 'R8');
$l9 = addLeft($treeArray, $r8, 'L9');

echo 'Tree array: ';
print_r($treeArray);
echo '-----------------' . PHP_EOL;

// === Build Tree

/**
 * Class Node
 */
class Node
{
    /**
     * @var array
     */
    public $data;

    /**
     * @var Node
     */
    public $left;

    /**
     * @var Node
     */
    public $right;

    /**
     * Node constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
}

/**
 * @param array $treeArray
 * @param int $position
 *
 * @return array
 */
function _searchItem(array $treeArray, int $position)
{
    foreach ($treeArray as $item) {
        if ($item['position'] === $position) {
            return $item;
        }
    }

    return null;
}

/**
 * @param array $treeArray
 * @param Node|null $root
 * @param int $position
 * @param int $maxPosition
 *
 * @return Node|null
 */
function _buildSubTree(array $treeArray, ?Node $root, int $position, int $maxPosition): ?Node
{
    if ($position > $maxPosition) {
        return $root;
    }

    $data = _searchItem($treeArray, $position);
    if (!$data) {
        return null;
    }

    $node = new Node($data);

    $root = $node;
    $root->left = _buildSubTree($treeArray, $root->left, 2 * $position + 1, $maxPosition);
    $root->right = _buildSubTree($treeArray, $root->right, 2 * $position + 2, $maxPosition);

    return $root;
}

/**
 * @param array $treeArray
 * @return Node
 */
function buildTree(array $treeArray): ?Node
{
    $max = array_reduce($treeArray, function ($carry, $item) {
        return max($carry, $item['position']);
    }, -1);

    return _buildSubTree($treeArray, null, 0, $max);
}

function printTree(Node $root)
{
    $printNode = function (?Node $node, int $indent) use (&$printNode) {
        if ($node === null) {
            return;
        }

        $name = $node->data['name'];
        $id = $node->data['id'];
        $ownerId = $node->data['ownerId'] ?? 'nil';
        $title = "{$name} id:{$id},owner:{$ownerId}";

        echo str_repeat('..', $indent) . $title . PHP_EOL;
        $printNode($node->left, $indent + 1);
        $printNode($node->right, $indent + 1);
    };

    $printNode($root, 0);
}

$tree = buildTree($treeArray);
print_r($tree);
echo '-----------------' . PHP_EOL;
printTree($tree);