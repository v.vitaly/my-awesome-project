<?php

require_once __DIR__ . '/vendor/autoload.php';

$calc = new \App\Calculation();

// 5+(8/2)*x = ?

echo $calc
        ->numbers([5, 8, 2, 'x'])// используемые по порядку числа
        ->actions('1+(2/3)*4')// используемые по порядку действия
        ->comparison(4)// значение x
        ->execute() . PHP_EOL;            // выводим ответ