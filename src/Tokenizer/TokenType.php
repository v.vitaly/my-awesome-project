<?php
declare(strict_types=1);

namespace App\Tokenizer;

/**
 * Tokens types reference
 */
class TokenType
{
    public const EOF = 0;
    public const OPERAND = 1;
    public const BRACKET_OPEN = 2;
    public const BRACKET_CLOSE = 3;
    public const WHITESPACE = 4;
    public const MUL = 5;
    public const DIV = 6;
    public const PLUS = 7;
    public const MINUS = 8;
}