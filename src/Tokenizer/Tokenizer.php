<?php
declare(strict_types=1);

namespace App\Tokenizer;

use App\Reader\ReaderInterface;

/**
 * Tokenize input
 */
class Tokenizer implements TokenizerInterface
{
    /**
     * @var ReaderInterface
     */
    private $reader;

    /**
     * @var Token
     */
    private $token;

    /**
     * Lexer constructor.
     * @param ReaderInterface $reader
     */
    public function __construct(ReaderInterface $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @return Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * Get next token from input
     *
     * @return Token
     */
    public function next(): Token
    {
        while (($char = $this->reader->getChar()) !== null) {
            $this->token = $this->tryOperator($char);
            if ($this->token) {
                return $this->token;
            }

            $this->token = $this->tryOperand($char);
            if ($this->token) {
                return $this->token;
            }

            $this->token = $this->tryBrackets($char);
            if ($this->token) {
                return $this->token;
            }

            $this->token = $this->tryWhitespaces($char);
            if ($this->token) {
                return $this->token;
            }

            if (!$this->token) {
                throw new UnknownTokenException($char, "Can not tokenize \"$char\"");
            }
        }

        return new Token(TokenType::EOF, null);
    }

    /**
     * Try to tokenize operator
     *
     * @param string $char
     * @return Token|null
     */
    private function tryOperator(string  $char): ?Token
    {
        static $types = [
            '*' => TokenType::MUL,
            '/' => TokenType::DIV,
            '+' => TokenType::PLUS,
            '-' => TokenType::MINUS,
        ];

        return isset($types[$char]) ? new Token($types[$char], $char) : null;
    }

    /**
     * Try to tokenize operand (must be numeric - it's just indexes)
     *
     * @param string $char
     *
     * @return Token|null
     */
    private function tryOperand(string $char): ?Token
    {
        $isOperand = function (string  $char): bool {
            return is_numeric($char);
        };

        if (!$isOperand($char)) {
            return null;
        }

        $val = $char;
        while (($char = $this->reader->getChar()) !== null) {
            if (!$isOperand($char)) {
                break;
            }
            $val .= $char;
        }
        if ($char) {
            $this->reader->revertChar();
        }
        return new Token(TokenType::OPERAND, $val);
    }

    /**
     * Try to tokenize brackets
     *
     * @param string $char
     *
     * @return Token|null
     */
    private function tryBrackets(string $char): ?Token
    {
        if ($char === '(') {
            return new Token(TokenType::BRACKET_OPEN, $char);
        }

        if ($char === ')') {
            return new Token(TokenType::BRACKET_CLOSE, $char);
        }

        return null;
    }

    /**
     * Try to tokenize one or more whitespaces
     *
     * @param string $char
     *
     * @return Token|null
     */
    private function tryWhitespaces(string $char): ?Token
    {
        $isSpace = function (string  $char): bool {
            return $char === ' ' || $char === "\t";
        };

        if (!$isSpace($char)) {
            return null;
        }

        $val = $char;
        while (($char = $this->reader->getChar()) !== null) {
            if (!$isSpace($char)) {
                break;
            }
            $val .= $char;
        }
        if ($char) {
            $this->reader->revertChar();
        }
        return new Token(TokenType::WHITESPACE, $val);
    }
}