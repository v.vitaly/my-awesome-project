<?php
declare(strict_types=1);


namespace App\Tokenizer;

/**
 * Interface LexerInterface
 */
interface TokenizerInterface
{
    /**
     * Get next token from input
     *
     * @return Token
     */
    public function next(): Token;
}