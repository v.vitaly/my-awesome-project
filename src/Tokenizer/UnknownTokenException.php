<?php
declare(strict_types=1);

namespace App\Tokenizer;

use Throwable;

/**
 * Class UnknownTokenException
 */
class UnknownTokenException extends \DomainException
{
    public $token;

    /**
     * UnknownTokenException constructor.
     * @param string $token
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $token, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->token = $token;
    }
}