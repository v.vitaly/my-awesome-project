<?php
declare(strict_types=1);

namespace App\Tokenizer;

/**
 * Class Token
 */
final class Token
{
    /**
     * @var int
     */
    private $type;

    /**
     * @var string|null
     */
    private $value;

    /**
     * Token constructor.
     * @param int $type
     * @param string $value
     */
    public function __construct(int $type, ?string $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * Check token type
     *
     * @param int $type
     *
     * @return bool
     */
    public function is(int $type): bool
    {
        return $this->type === $type;
    }

    /**
     * Check if token is operator
     *
     * @return bool
     */
    public function isOperator(): bool
    {
        return \in_array($this->type, [TokenType::MUL, TokenType::DIV, TokenType::PLUS, TokenType::MINUS], true);
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }
}