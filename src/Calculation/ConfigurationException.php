<?php
declare(strict_types=1);

namespace App\Calculation;

/**
 * Class ConfigurationException
 */
class ConfigurationException extends \RuntimeException
{

}