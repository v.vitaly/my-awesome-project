<?php
declare(strict_types=1);

namespace App;

use App\Calculation\ConfigurationException;
use App\Interpreter\EvaluateInterpreter;
use App\Interpreter\EvaluationResult;
use App\Parser\Parser;
use App\Parser\ParserInterface;
use App\Reader\StringReader;
use App\Tokenizer\Tokenizer;

/**
 * Class Calculation
 */
class Calculation
{
    /**
     * Unknown variables
     *
     * @var int[]
     */
    private $variables;

    /**
     * @var string
     */
    private $statement;

    /**
     * @var EvaluateInterpreter
     */
    private $evaluator;

    /**
     * @var ParserInterface
     */
    private $parser;

    /**
     * Calculation constructor.
     */
    public function __construct()
    {
        $this->parser = new Parser();
    }

    /**
     * Set operands by order
     *
     * @param array $input
     *
     * @return Calculation
     */
    public function numbers(array $input): Calculation
    {
        $this->evaluator = new EvaluateInterpreter();
        $this->variables = [];

        foreach ($input as $i => $value) {
            $index = $i + 1;

            if (is_string($value)) { // variable
                $this->variables[] = $index;
            } else {
                $this->evaluator->addValue($index, (float)$value);
            }
        }

        return $this;
    }

    /**
     * Set expression to dod
     *
     * @param string $statement
     *
     * @return Calculation
     */
    public function actions(string $statement): Calculation
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * Set variable
     *
     * @param float $value
     *
     * @return Calculation
     */
    public function comparison(float $value): Calculation
    {
        if (!$this->evaluator) {
            throw new ConfigurationException('Can not set variables before numbers');
        }

        if (isset($this->variables[0])) {
            $this->evaluator->addValue($this->variables[0], $value);
        }

        return $this;
    }

    /**
     * Execute statement
     *
     * @return float
     */
    public function execute(): float
    {
        if (!$this->statement) {
            throw new ConfigurationException('Statement does not set');
        }

        if (!$this->evaluator) {
            throw new ConfigurationException('Can not execute without numbers');
        }

        $reader = new StringReader($this->statement);
        $tokenizer = new Tokenizer($reader);

        $root = $this->parser->parse($tokenizer);

        /** @var EvaluationResult $result */
        $result = $root->interpret($this->evaluator);

        return $result->getResult();
    }
}