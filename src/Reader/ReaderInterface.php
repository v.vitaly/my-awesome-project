<?php
declare(strict_types=1);

namespace App\Reader;

/**
 * Read input
 */
interface ReaderInterface
{
    /**
     * Get the next character from input. Returns NULL if there are no chars in input.
     *
     * @return string|null
     */
    public function getChar(): ?string;

    /**
     * Push the read char back
     *
     * @return void
     */
    public function revertChar(): void;
}