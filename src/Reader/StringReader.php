<?php
declare(strict_types=1);

namespace App\Reader;

/**
 * Read input from string
 */
class StringReader implements ReaderInterface
{
    /**
     * @var string
     */
    private $input;

    /**
     * @var int
     */
    private $currentPosition;

    /**
     * @var string
     */
    private $length;

    /**
     * StringReader constructor.
     * @param string $input
     */
    public function __construct(string $input)
    {
        $this->input = $input;
        $this->currentPosition = 0;
        $this->length = strlen($input);
    }

    /**
     * @inheritDoc
     */
    public function getChar(): ?string
    {
        if ($this->currentPosition >= $this->length) {
            return null;
        }

        $char = $this->input[$this->currentPosition];
        $this->currentPosition++;
        return $char;
    }

    /**
     * @inheritDoc
     */
    public function revertChar(): void
    {
        $this->currentPosition--;
    }
}