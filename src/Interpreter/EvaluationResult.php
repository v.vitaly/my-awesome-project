<?php
declare(strict_types=1);

namespace App\Interpreter;

/**
 * Class EvaluationResult
 */
class EvaluationResult implements InterpretationResult
{
    /**
     * @var float
     */
    private $value;

    /**
     * EvaluationResult constructor.
     * @param float $value
     */
    public function __construct(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return int|float
     */
    public function getResult()
    {
        return $this->value;
    }
}