<?php
declare(strict_types=1);

namespace App\Interpreter;

use App\Parser\Calc\Operators\Addition;
use App\Parser\Calc\Operators\Division;
use App\Parser\Calc\Operators\LeftBracket;
use App\Parser\Calc\Operators\Multiplication;
use App\Parser\Calc\Operators\Subtraction;
use App\Parser\Calc\Value;

/**
 * Interpret each node in build tree
 */
interface InterpreterInterface
{
    /**
     * Interpret single value
     *
     * @param Value $calc
     *
     * @return InterpretationResult
     */
    public function interpretValue(Value $calc): InterpretationResult;

    /**
     * Interpret addition operation
     *
     * @param Addition $calc
     *
     * @return InterpretationResult
     */
    public function interpretAddition(Addition $calc): InterpretationResult;

    /**
     * Interpret subtraction operation
     *
     * @param Subtraction $calc
     *
     * @return InterpretationResult
     */
    public function interpretSubtraction(Subtraction $calc): InterpretationResult;

    /**
     * Interpret division operation
     *
     * @param Division $calc
     *
     * @return InterpretationResult
     */
    public function interpretDivision(Division $calc): InterpretationResult;

    /**
     * Interpret multiplication operation
     *
     * @param Multiplication $calc
     *
     * @return InterpretationResult
     */
    public function interpretMultiplication(Multiplication $calc): InterpretationResult;

    /**
     * @param LeftBracket $calc
     *
     * @return InterpretationResult
     */
    public function interpretLeftBracket(LeftBracket $calc): InterpretationResult;

    /**
     * @param LeftBracket $calc
     *
     * @return InterpretationResult
     */
    public function interpretRightBracket(LeftBracket $calc): InterpretationResult;
}