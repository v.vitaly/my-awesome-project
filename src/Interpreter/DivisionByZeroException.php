<?php
declare(strict_types=1);

namespace App\Interpreter;

/**
 * Class DivisionByZeroException
 */
class DivisionByZeroException extends \DomainException
{

}