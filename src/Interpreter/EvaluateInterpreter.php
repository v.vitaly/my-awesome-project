<?php
declare(strict_types=1);

namespace App\Interpreter;

use App\Parser\Calc\Operators\Addition;
use App\Parser\Calc\Operators\Division;
use App\Parser\Calc\Operators\LeftBracket;
use App\Parser\Calc\Operators\Multiplication;
use App\Parser\Calc\Operators\Subtraction;
use App\Parser\Calc\Value;

/**
 * Evaluate given statement
 */
class EvaluateInterpreter implements InterpreterInterface
{
    /**
     * Operands values
     *
     * @var int[]|float[]
     */
    private $values = [];

    /**
     * @param int $index
     * @param float $value
     *
     * @return void
     */
    public function addValue(int $index, float $value): void
    {
        $this->values[$index] = $value;
    }

    /**
     * @inheritDoc
     */
    public function interpretValue(Value $calc): InterpretationResult
    {
        $index = $calc->getIndex();

        if (!array_key_exists($index, $this->values)) {
            throw new UnknownVariableException("Unknown variable #{$index}");
        }

        return new EvaluationResult($this->values[$index]);
    }

    /**
     * @inheritDoc
     */
    public function interpretAddition(Addition $calc): InterpretationResult
    {
        /** @var EvaluationResult $leftValue */
        $leftValue = $calc->getLeftOperand()->interpret($this);

        /** @var EvaluationResult $rightValue */
        $rightValue = $calc->getRightOperand()->interpret($this);

        return new EvaluationResult($leftValue->getResult() + $rightValue->getResult());
    }

    /**
     * @inheritDoc
     */
    public function interpretSubtraction(Subtraction $calc): InterpretationResult
    {
        /** @var EvaluationResult $leftValue */
        $leftValue = $calc->getLeftOperand()->interpret($this);

        /** @var EvaluationResult $rightValue */
        $rightValue = $calc->getRightOperand()->interpret($this);

        return new EvaluationResult($leftValue->getResult() - $rightValue->getResult());
    }

    /**
     * @inheritDoc
     */
    public function interpretDivision(Division $calc): InterpretationResult
    {
        /** @var EvaluationResult $rightValue */
        $rightValue = $calc->getRightOperand()->interpret($this);
        $rightValue = $rightValue->getResult();

        if ((int)$rightValue === 0) {
            throw new DivisionByZeroException('Can not divide by zero');
        }

        /** @var EvaluationResult $leftValue */
        $leftValue = $calc->getLeftOperand()->interpret($this);

        return new EvaluationResult($leftValue->getResult() / $rightValue);
    }

    /**
     * @inheritDoc
     */
    public function interpretMultiplication(Multiplication $calc): InterpretationResult
    {
        /** @var EvaluationResult $leftValue */
        $leftValue = $calc->getLeftOperand()->interpret($this);

        /** @var EvaluationResult $rightValue */
        $rightValue = $calc->getRightOperand()->interpret($this);

        return new EvaluationResult($leftValue->getResult() * $rightValue->getResult());
    }

    /**
     * @inheritDoc
     */
    public function interpretLeftBracket(LeftBracket $calc): InterpretationResult
    {
        return new NoneInterpretation();
    }

    /**
     * @inheritDoc
     */
    public function interpretRightBracket(LeftBracket $calc): InterpretationResult
    {
        return new NoneInterpretation();
    }
}