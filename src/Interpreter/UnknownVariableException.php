<?php
declare(strict_types=1);

namespace App\Interpreter;

/**
 * Class UnknownVariableException
 */
class UnknownVariableException extends \DomainException
{

}