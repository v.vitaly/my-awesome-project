<?php
declare(strict_types=1);


namespace App\Parser;

use App\Parser\Calc\CalcInterface;
use App\Tokenizer\TokenizerInterface;

/**
 * Build parse tree
 */
interface ParserInterface
{
    /**
     * Parse given token into the build tree
     *
     * @param TokenizerInterface $tokenizer
     *
     * @return CalcInterface
     *
     * @throws ParsingException
     */
    public function parse(TokenizerInterface $tokenizer): CalcInterface;
}