<?php
declare(strict_types=1);

namespace App\Parser;

/**
 * Raised when parser can not find corresponding bracket
 */
class MissedBracketException extends ParsingException
{
}