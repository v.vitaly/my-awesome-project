<?php
declare(strict_types=1);

namespace App\Parser;

/**
 * Class MismatchBracketException
 */
class MismatchBracketException extends ParsingException
{

}