<?php
declare(strict_types=1);

namespace App\Parser;

/**
 * Class PasringException
 */
class ParsingException extends \DomainException
{
}