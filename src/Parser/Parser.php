<?php
declare(strict_types=1);

namespace App\Parser;

use App\Parser\Calc\CalcInterface;
use App\Parser\Calc\Factory;
use App\Parser\Calc\Operators\BinaryCalcOperator;
use App\Parser\Calc\Operators\LeftBracket;
use App\Parser\Calc\Operators\RightBracket;
use App\Tokenizer\TokenizerInterface;
use App\Tokenizer\TokenType;

/**
 * Build parse tree
 */
class Parser implements ParserInterface
{
    /**
     * @param TokenizerInterface $tokenizer
     *
     * @return CalcInterface
     *
     * @throws MismatchBracketException
     * @throws SyntaxErrorException
     */
    public function parse(TokenizerInterface $tokenizer): CalcInterface
    {
        $stack = new \SplStack();
        $output = new \SplQueue();

        while (!($token = $tokenizer->next())->is(TokenType::EOF)) {
            if ($token->is(TokenType::WHITESPACE)) {
                continue;
            }

            $calc = Factory::resolveCalculation($token);

            if ($calc->isTerminal()) {
                $output->enqueue($calc);
            } elseif ($calc instanceof BinaryCalcOperator) {

                while (!$stack->isEmpty()) {
                    /** @var BinaryCalcOperator $topOperator */
                    $topOperator = $stack->top();

                    $canPush =
                        (!$topOperator instanceof LeftBracket) &&
                        (
                            $topOperator->isGreaterPrecedence($calc) ||
                            ($topOperator->isEqualPrecedence($calc) && $topOperator->isLeftAssociative())
                        );

                    if (!$canPush) {
                        break;
                    }

                    $topOperator = $stack->pop();
                    $this->parseExpression($topOperator, $output);
                    $output->enqueue($topOperator);
                }

                $stack->push($calc);

            } elseif ($calc instanceof LeftBracket) {
                $stack->push($calc);
            } elseif ($calc instanceof RightBracket) {
                $closed = false;

                while (!$stack->isEmpty()) {

                    /** @var BinaryCalcOperator $topCalculation */
                    $topOperator = $stack->pop();
                    if ($topOperator instanceof LeftBracket) {
                        $closed = true;
                        break;
                    }

                    $this->parseExpression($topOperator, $output);
                    $output->enqueue($topOperator);
                }

                if (!$closed) {
                    throw new MissedBracketException('Bracket is missing');
                }
            }
        }

        while(!$stack->isEmpty()) {
            $topOperator = $stack->pop();

            if ($topOperator instanceof LeftBracket) {
                throw new MismatchBracketException('Brackets are mismatch');
            }

            $this->parseExpression($topOperator, $output);
            $output->enqueue($topOperator);
        }

        if ($output->count() > 1) {
            throw new SyntaxErrorException('Can not parse statement');
        }

        return $output->dequeue();
    }

    /**
     * @param BinaryCalcOperator $operator
     * @param \SplQueue $output
     * @return void
     */
    private function parseExpression(BinaryCalcOperator $operator, \SplQueue $output): void
    {
        $left = $right = null;

        try {
            $right = $output->pop();
            $left = $output->pop();
        } catch (\RuntimeException $exception) {
        }

        if ($right === null || $left === null) {
            throw new SyntaxErrorException("Wrong operator \"{$operator->getOperator()}\" syntax");
        }

        $operator->assignOperands($left, $right);
    }
}