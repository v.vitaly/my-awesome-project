<?php
declare(strict_types=1);

namespace App\Parser\Calc;

use App\Parser\Calc\Operators\Addition;
use App\Parser\Calc\Operators\Division;
use App\Parser\Calc\Operators\LeftBracket;
use App\Parser\Calc\Operators\Multiplication;
use App\Parser\Calc\Operators\RightBracket;
use App\Parser\Calc\Operators\Subtraction;
use App\Tokenizer\Token;
use App\Tokenizer\TokenType;

/**
 * Class Factory
 */
class Factory
{
    /**
     * @param Token $token
     *
     * @return CalcInterface|null
     */
    public static function resolveCalculation(Token $token): ?CalcInterface
    {
        if ($token->is(TokenType::BRACKET_OPEN)) {
            return new LeftBracket();
        }

        if ($token->is(TokenType::BRACKET_CLOSE)) {
            return new RightBracket();
        }

        if ($token->is(TokenType::OPERAND)) {
            return new Value((int) $token->getValue());
        }

        if ($token->is(TokenType::MUL)) {
            return new Multiplication($token->getValue());
        }

        if ($token->is(TokenType::DIV)) {
            return new Division($token->getValue());
        }

        if ($token->is(TokenType::PLUS)) {
            return new Addition($token->getValue());
        }

        if ($token->is(TokenType::MINUS)) {
            return new Subtraction($token->getValue());
        }

        return null;
    }
}