<?php
declare(strict_types=1);

namespace App\Parser\Calc;

use App\Interpreter\InterpretationResult;
use App\Interpreter\InterpreterInterface;

/**
 * Interface CalcInterface
 */
interface CalcInterface
{
    /**
     * Check if current node is terminal in parse tree
     *
     * @return bool
     */
    public function isTerminal(): bool;

    /**
     * Interpret current calculation with the given object
     *
     * @param InterpreterInterface $interpreter
     *
     * @return InterpretationResult
     */
    public function interpret(InterpreterInterface $interpreter): InterpretationResult;
}