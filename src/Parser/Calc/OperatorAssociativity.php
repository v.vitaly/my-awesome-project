<?php
declare(strict_types=1);

namespace App\Parser\Calc;

/**
 * Operator associativity
 */
final class OperatorAssociativity
{
    private const LEFT = 0;
    private const RIGHT = 1;

    /**
     * @var int
     */
    private $associativity;

    /**
     * OperatorAssociativity constructor.
     * @param int $associativity
     */
    private function __construct(int $associativity)
    {
        $this->associativity = $associativity;
    }

    /**
     * @return bool
     */
    public function isLeftAssociative(): bool
    {
        return $this->associativity === self::LEFT;
    }

    /**
     * @return OperatorAssociativity
     */
    public static function leftAssociative(): OperatorAssociativity
    {
        return new self(self::LEFT);
    }

    /**
     * @return OperatorAssociativity
     */
    public static function rightAssociative(): OperatorAssociativity
    {
        return new self(self::RIGHT);
    }
}