<?php
declare(strict_types=1);

namespace App\Parser\Calc\Operators;

use App\Interpreter\InterpretationResult;
use App\Interpreter\InterpreterInterface;

/**
 * Right bracket (handling brackets)
 */
class RightBracket implements CalcOperator
{
    /**
     * @inheritDoc
     */
    public function isTerminal(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getOperator(): string
    {
        return ')';
    }

    /**
     * @inheritDoc
     */
    public function interpret(InterpreterInterface $interpreter): InterpretationResult
    {
        return $interpreter->interpretRightBracket($this);
    }
}