<?php
declare(strict_types=1);

namespace App\Parser\Calc\Operators;

use App\Parser\Calc\CalcInterface;

/**
 * Interface CalcOperator
 */
interface CalcOperator extends CalcInterface
{
    /**
     * Get operator token
     *
     * @return string
     */
    public function getOperator(): string;
}