<?php
declare(strict_types=1);

namespace App\Parser\Calc\Operators;

use App\Interpreter\InterpretationResult;
use App\Interpreter\InterpreterInterface;
use App\Parser\Calc\OperatorAssociativity;

/**
 * Multiplication operation: <left> * <right>
 */
class Multiplication extends BinaryCalcOperator
{
    /**
     * @inheritDoc
     */
    protected function getOperatorPrecedence(): int
    {
        return 20;
    }

    /**
     * @inheritDoc
     */
    protected function getOperatorAssociativity(): OperatorAssociativity
    {
        return OperatorAssociativity::leftAssociative();
    }

    /**
     * @inheritDoc
     */
    public function interpret(InterpreterInterface $interpreter): InterpretationResult
    {
        return $interpreter->interpretMultiplication($this);
    }
}