<?php
declare(strict_types=1);

namespace App\Parser\Calc\Operators;

use App\Parser\Calc\CalcInterface;
use App\Parser\Calc\OperatorAssociativity;
use App\Parser\SyntaxErrorException;

/**
 * Abstract calculation
 */
abstract class BinaryCalcOperator implements CalcOperator
{
    /**
     * @var string
     */
    private $operator;

    /**
     * @var CalcInterface
     */
    private $left;

    /**
     * @var CalcInterface
     */
    private $right;

    /**
     * CalcOperator constructor.
     * @param $operator
     */
    public function __construct($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return int
     */
    abstract protected function getOperatorPrecedence(): int;

    /**
     * @return OperatorAssociativity
     */
    abstract protected function getOperatorAssociativity(): OperatorAssociativity;

    /**
     * Get operator token
     *
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @inheritDoc
     */
    public function isTerminal(): bool
    {
        return false;
    }

    /**
     * @return CalcInterface
     * @throws SyntaxErrorException
     */
    public function getLeftOperand(): CalcInterface
    {
        if ($this->left === null) {
            throw new SyntaxErrorException("Left operand for operator \"{$this->operator}\" is not parsed");
        }
        return $this->left;
    }

    /**
     * @return CalcInterface
     * @throws SyntaxErrorException
     */
    public function getRightOperand(): CalcInterface
    {
        if ($this->left === null) {
            throw new SyntaxErrorException("Right operand for operator \"{$this->operator}\" is not parsed");
        }
        return $this->right;
    }

    /**
     * @param CalcInterface $left
     * @param CalcInterface $right
     */
    public function assignOperands(CalcInterface $left, CalcInterface $right): void
    {
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * Check if precedence of the current operator is greater than other's one
     *
     * @param BinaryCalcOperator $other
     *
     * @return bool
     */
    public function isGreaterPrecedence(BinaryCalcOperator $other): bool
    {
        return $this->getOperatorPrecedence() > $other->getOperatorPrecedence();
    }

    /**
     * Check if precedence of the current operator is equals to other's one
     *
     * @param BinaryCalcOperator $other
     *
     * @return bool
     */
    public function isEqualPrecedence(BinaryCalcOperator $other): bool
    {
        return $this->getOperatorPrecedence() === $other->getOperatorPrecedence();
    }

    /**
     * Check if current operator is left-associative
     *
     * @return bool
     */
    public function isLeftAssociative(): bool
    {
        return $this->getOperatorAssociativity()->isLeftAssociative();
    }
}