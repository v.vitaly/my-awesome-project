<?php
declare(strict_types=1);

namespace App\Parser\Calc;

use App\Interpreter\InterpretationResult;
use App\Interpreter\InterpreterInterface;

/**
 * Single operand value
 */
class Value implements CalcInterface
{
    /**
     * @var int
     */
    private $index;

    /**
     * Value constructor.
     * @param int $index
     */
    public function __construct(int $index)
    {
        $this->index = $index;
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @inheritDoc
     */
    public function isTerminal(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function interpret(InterpreterInterface $interpreter): InterpretationResult
    {
        return $interpreter->interpretValue($this);
    }
}