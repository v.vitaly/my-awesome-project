<?php
declare(strict_types=1);

namespace App\Parser;

/**
 * Wrong syntax
 */
class SyntaxErrorException extends ParsingException
{
}