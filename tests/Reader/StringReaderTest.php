<?php
declare(strict_types=1);

namespace tests\Reader;

use App\Reader\StringReader;
use PHPUnit\Framework\TestCase;

/**
 * Class StringReaderTest
 */
class StringReaderTest extends TestCase
{
    public function testGetChar()
    {
        $reader = new StringReader('test');

        self::assertEquals('t', $reader->getChar());
        self::assertEquals('e', $reader->getChar());
    }

    public function testGetCharWhenAllRead()
    {
        $reader = new StringReader('');

        self::assertNull($reader->getChar());
    }

    public function testRevertChar()
    {
        $reader = new StringReader('123');

        $reader->getChar();
        $reader->revertChar();

        self::assertEquals('1', $reader->getChar());
    }
}