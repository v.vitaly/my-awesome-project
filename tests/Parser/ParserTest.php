<?php
declare(strict_types=1);

namespace tests\Parser;

use App\Parser\Calc\Operators\Addition;
use App\Parser\Calc\Operators\Multiplication;
use App\Parser\Calc\Operators\Subtraction;
use App\Parser\Calc\Value;
use App\Parser\MismatchBracketException;
use App\Parser\MissedBracketException;
use App\Parser\Parser;
use App\Parser\SyntaxErrorException;
use App\Tokenizer\Token;
use App\Tokenizer\TokenizerInterface;
use App\Tokenizer\TokenType;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 */
class ParserTest extends TestCase
{
    public function testParseSingleValue(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $calc = $parser->parse($tokenizer);

        self::assertInstanceOf(Value::class, $calc);
        self::assertSame(1, $calc->getIndex());
    }

    public function testParseExpressions(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::MUL, '*'),
                new Token(TokenType::OPERAND, '2'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $calc = $parser->parse($tokenizer);

        self::assertInstanceOf(Multiplication::class, $calc);
        self::assertInstanceOf(Value::class, $calc->getLeftOperand());
        self::assertSame(1, $calc->getLeftOperand()->getIndex());
        self::assertInstanceOf(Value::class, $calc->getRightOperand());
        self::assertSame(2, $calc->getRightOperand()->getIndex());
    }

    public function testParseShouldConsiderPrecedence(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::MINUS, '-'),
                new Token(TokenType::OPERAND, '2'),
                new Token(TokenType::MUL, '*'),
                new Token(TokenType::OPERAND, '3'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $calc = $parser->parse($tokenizer);

        self::assertInstanceOf(Subtraction::class, $calc);

        self::assertInstanceOf(Value::class, $calc->getLeftOperand());
        self::assertSame(1, $calc->getLeftOperand()->getIndex());

        self::assertInstanceOf(Multiplication::class, $calc->getRightOperand());
        self::assertInstanceOf(Value::class, $calc->getRightOperand()->getLeftOperand());
        self::assertSame(2, $calc->getRightOperand()->getLeftOperand()->getIndex());
        self::assertInstanceOf(Value::class, $calc->getRightOperand()->getRightOperand());
        self::assertSame(3, $calc->getRightOperand()->getRightOperand()->getIndex());
    }

    public function testParseWithBrackets(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::MUL, '*'),
                new Token(TokenType::BRACKET_OPEN, '('),
                new Token(TokenType::OPERAND, '2'),
                new Token(TokenType::PLUS, '+'),
                new Token(TokenType::OPERAND, '3'),
                new Token(TokenType::BRACKET_CLOSE, ')'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $calc = $parser->parse($tokenizer);

        self::assertInstanceOf(Multiplication::class, $calc);

        self::assertInstanceOf(Value::class, $calc->getLeftOperand());
        self::assertSame(1, $calc->getLeftOperand()->getIndex());

        self::assertInstanceOf(Addition::class, $calc->getRightOperand());
        self::assertInstanceOf(Value::class, $calc->getRightOperand()->getLeftOperand());
        self::assertSame(2, $calc->getRightOperand()->getLeftOperand()->getIndex());
        self::assertInstanceOf(Value::class, $calc->getRightOperand()->getRightOperand());
        self::assertSame(3, $calc->getRightOperand()->getRightOperand()->getIndex());
    }

    public function testParseShouldSkipWhitespaces(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::PLUS, '+'),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::OPERAND, '2'),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::WHITESPACE, ' '),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $calc = $parser->parse($tokenizer);

        self::assertInstanceOf(Addition::class, $calc);
        self::assertInstanceOf(Value::class, $calc->getLeftOperand());
        self::assertSame(1, $calc->getLeftOperand()->getIndex());
        self::assertInstanceOf(Value::class, $calc->getRightOperand());
        self::assertSame(2, $calc->getRightOperand()->getIndex());
    }

    public function testParseWithBracketsShouldErrorIfMissedBracket(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::BRACKET_OPEN, '('),
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $this->expectException(MismatchBracketException::class);
        $parser->parse($tokenizer);
    }

    public function testParseWithBracketsShouldErrorIfMismatchBrackets(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::BRACKET_CLOSE, ')'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $this->expectException(MissedBracketException::class);
        $parser->parse($tokenizer);
    }

    public function testParseWhenSyntaxError(): void
    {
        $tokenizer = $this->createMock(TokenizerInterface::class);

        $tokenizer->method('next')
            ->willReturnOnConsecutiveCalls(
                new Token(TokenType::OPERAND, '1'),
                new Token(TokenType::MUL, '*'),
                new Token(TokenType::EOF, null)
            );

        $parser = new Parser();

        $this->expectException(SyntaxErrorException::class);
        $parser->parse($tokenizer);
    }
}