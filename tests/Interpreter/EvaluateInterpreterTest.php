<?php
declare(strict_types=1);

namespace tests\Interpreter;

use App\Interpreter\DivisionByZeroException;
use App\Interpreter\EvaluateInterpreter;
use App\Interpreter\UnknownVariableException;
use App\Parser\Calc\Operators\Addition;
use App\Parser\Calc\Operators\Division;
use App\Parser\Calc\Operators\Multiplication;
use App\Parser\Calc\Operators\Subtraction;
use App\Parser\Calc\Value;
use PHPUnit\Framework\TestCase;

/**
 * Class EvaluateInterpreterTest
 */
class EvaluateInterpreterTest extends TestCase
{
    public function testInterpretValue(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 12.34);

        $actual = $eval->interpretValue(new Value(1));

        self::assertEquals(12.34, $actual->getResult());
    }

    public function testInterpretValueIfVariableDoesNotSet(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 12.34);

        $this->expectException(UnknownVariableException::class);
        $eval->interpretValue(new Value(3));
    }

    public function testInterpretAddition(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 1.2);
        $eval->addValue(2, 3.4);

        $op = new Addition('+');
        $op->assignOperands(new Value(1), new Value(2));
        $actual = $eval->interpretAddition($op);

        self::assertSame(4.6, $actual->getResult());
    }

    public function testInterpretSubtraction(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 5);
        $eval->addValue(2, 2.5);

        $op = new Subtraction('-');
        $op->assignOperands(new Value(1), new Value(2));
        $actual = $eval->interpretSubtraction($op);

        self::assertSame(2.5, $actual->getResult());
    }

    public function testInterpretDivision(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 5);
        $eval->addValue(2, 2);

        $op = new Division('/');
        $op->assignOperands(new Value(1), new Value(2));
        $actual = $eval->interpretDivision($op);

        self::assertSame(2.5, $actual->getResult());
    }

    public function testInterpretDivisionWithZero(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 5);
        $eval->addValue(2, 0);

        $op = new Division('/');
        $op->assignOperands(new Value(1), new Value(2));

        $this->expectException(DivisionByZeroException::class);
        $eval->interpretDivision($op);
    }

    public function testInterpretMultiplication(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 2.5);
        $eval->addValue(2, 2);

        $op = new Multiplication('*');
        $op->assignOperands(new Value(1), new Value(2));
        $actual = $eval->interpretMultiplication($op);

        self::assertSame(5.0, $actual->getResult());
    }

    public function testInterpretExpression(): void
    {
        $eval = new EvaluateInterpreter();
        $eval->addValue(1, 2);
        $eval->addValue(2, 4);
        $eval->addValue(3, 1);
        $eval->addValue(4, 9);
        $eval->addValue(5, 3);

        // x1 * (x2 + x3) - x4 / x5
        // 2 * (4+1) - 9/3 = 7
        $root = new Subtraction('-');

        $mult = new Multiplication('*');

        $sum = new Addition('+');
        $sum->assignOperands(new Value(2), new Value(3));

        $mult->assignOperands(new Value(1), $sum);

        $div = new Division('/');
        $div->assignOperands(new Value(4), new Value(5));

        $root->assignOperands($mult, $div);

        $actual = $eval->interpretSubtraction($root);

        self::assertSame(7.0, $actual->getResult());
    }
}