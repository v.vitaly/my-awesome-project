<?php
declare(strict_types=1);

namespace tests\Tokenizer;

use App\Tokenizer\Tokenizer;
use App\Tokenizer\Token;
use App\Tokenizer\TokenType;
use App\Tokenizer\UnknownTokenException;
use App\Reader\StringReader;
use PHPUnit\Framework\TestCase;

/**
 * Class LexerTest
 */
class TokenizerTest extends TestCase
{
    /**
     * @return array
     */
    public function providerGetNextToken()
    {
        return [
            'empty' => ['', TokenType::EOF, null],
            'operand 1' => ['1', TokenType::OPERAND, '1'],
            'operand 121' => ['121', TokenType::OPERAND, '121'],
            '(' => ['(', TokenType::BRACKET_OPEN, '('],
            ')' => [')', TokenType::BRACKET_CLOSE, ')'],
            ' ' => [' ', TokenType::WHITESPACE, ' '],
            'tab' => ["\t", TokenType::WHITESPACE, "\t"],
            'some spaces' => ['   ', TokenType::WHITESPACE, '   '],
            '*' => ['*', TokenType::MUL, '*'],
            '/' => ['/', TokenType::DIV, '/'],
            '+' => ['+', TokenType::PLUS, '+'],
            '-' => ['-', TokenType::MINUS, '-'],
        ];
    }

    /**
     * @param string $input
     * @param int $expectedType
     * @param string $expectedToken
     *
     * @dataProvider providerGetNextToken
     */
    public function testNextToken($input, $expectedType, $expectedToken): void
    {
        $lexer = new Tokenizer(new StringReader($input));

        $token = $lexer->next();

        self::assertTrue($token->is($expectedType));
        self::assertSame($expectedToken, $token->getValue());
    }

    public function testNextTokenMultiple(): void
    {
        $lexer = new Tokenizer(new StringReader('2*2'));

        self::assertEquals($lexer->next(), new Token(TokenType::OPERAND, '2'));
        self::assertEquals($lexer->next(), new Token(TokenType::MUL, '*'));
    }

    public function testNextTokenShouldReturnEndTokenWhenFinish(): void
    {
        $lexer = new Tokenizer(new StringReader('2'));

        self::assertEquals($lexer->next(), new Token(TokenType::OPERAND, '2'));
        self::assertEquals($lexer->next(), new Token(TokenType::EOF, null));
    }

    public function testNextTokenShouldExceptionIfUnknownToken(): void
    {
        $lexer = new Tokenizer(new StringReader('?'));

        $this->expectException(UnknownTokenException::class);
        $lexer->next();
    }
}