Для проверки первой задачи необходимо запустить
```
php run.php
```

Скрипт решает указанное в задаче выражение. Требуемые классы расположены в `src/Parser/Calc/Operators`:
* `Addition`
* `Subtraction`
* `Multiplication`
* `Division`

Абстрактный родительский класс - `BinaryCalcOperator`.

В задании сделано допущение, что все числя имеют тип `float`.

----

Для проверки второй задачи необходимо запустить
```
php tree.php
```

В задаче строится дерево
```
       ROOT(1)
      /    \
     L2     R4
    /  \   /  \
   L3  R6 L5  R8
         \    /
         R7  L9
```
Первая часть задачи строит массив вида `[id, ownerId, name, position]`, где
* `id` генерируется автоматически при создании нового элемента
* `ownerId` - это ID родительского узла (`null` - для корня)
* `name` просто текст
* `position` - это целое число, описывающее позицию узла в дереве, как если бы оно хранилось в массиве:
  * позиция корня - 0
  * позиция левого потомка: `2 * position + 1`
  * позиция правого потомка: `2 * position + 2`
  
Во второй части задачи, рекурсивно строится дерево на основе полученного массива.